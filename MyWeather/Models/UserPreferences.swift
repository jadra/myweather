//
//  UserPreferences.swift
//  MyWeather
//
//  Created by john on 6/2/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

protocol UserPreferences {
    var unitSystem: Int { get set }
    var currentLocation: Int { get set }
}


class Preferences: UserPreferences {
    
    struct Keys {
        static let UnitSystemKey = "UnitSystem"
        static let CurrentLocationIdKey = "CurrentLocationId"
    }
    
    private var defaults: UserDefaults
    
    var unitSystem: Int {
        get {
            return defaults.integer(forKey: Keys.UnitSystemKey)
        }
        set {
            defaults.set(newValue, forKey: Keys.UnitSystemKey)
        }
    }
    
    var currentLocation: Int {
        get {
            return defaults.integer(forKey: Keys.CurrentLocationIdKey)
        }
        set {
            defaults.set(newValue, forKey: Keys.CurrentLocationIdKey)
        }
    }
    
    init() {
        defaults = UserDefaults.standard
    }
}
