//
//  WeatherDataModel.swift
//  MyWeather
//
//  Created by john on 5/17/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation
import FMDB

typealias LocationSearchResults = (Error?, Location?) -> Void
typealias LocationsSearchResults = (Error?, [Location]?) -> Void
typealias WeatherForecastResult = (Error?, WeatherForecast?) -> Void

enum WeatherDataModelError: Error {
    case malformedResponse
    case missingLocationName
}

protocol WeatherDataModel {
    var weatherNetworkService: WeatherService? { get set }
    var dataStore: DataStorage? { get set }
    func searchForLocation(query: String, _ completion: @escaping LocationsSearchResults)
    func getLocations(_ results: @escaping LocationsSearchResults)
    func getLocation(forId id: Int, _ completion: @escaping LocationSearchResults)
    func saveLocation(location: Location)
    func deleteLocation(location: Location, _ completion: @escaping (Error?) -> Void)
    func updateWeatherForecast(location: Location, _ completion: @escaping WeatherForecastResult)
}

struct WeatherDataModelNotifications {
    static let LocationWasSaved = Notification.Name("LocationWasSaved")
    static let LocationWasDeleted = Notification.Name("LocationWasDeleted")
    static let LocationKey = "location"
}

typealias DataModelNotifications = WeatherDataModelNotifications


final class WeatherDataService: WeatherDataModel {
    var weatherNetworkService: WeatherService?
    var dataStore: DataStorage?

    
    func searchForLocation(query: String, _ completion: @escaping LocationsSearchResults) {
        weatherNetworkService?.search(query: query) { (error, response) in
            if let error = error {
                completion(error, nil)
            }
            else {
                do {
                    guard let response = response else {
                        throw WeatherDataModelError.malformedResponse
                    }
                    let locations = try response.decodeFromJSON(into: Array<Location>.self)
                    completion(nil, locations)
                }
                catch {
                    completion(error, nil)
                }
            }
        }
    }
    
    
    func getLocations(_ results: @escaping LocationsSearchResults) {
        var locations: [Location]?
        dataStore?.read(statements: { db in
            let rs = try db.executeQuery(WeatherForecast.Queries.ReadLocations, values: nil)
            locations = []
            while rs.next() {
                let identifier = rs.long(forColumnIndex: 0)
                guard let name = rs.string(forColumn: Location.CodingKeys.name.rawValue) else {
                    throw  WeatherDataModelError.missingLocationName
                }
                let location = Location(identifier: identifier, name: name)
                locations?.append(location)
            }
            rs.close()
        }) { error in
            results(error,locations)
        }
    }
    
    func getLocation(forId id: Int, _ completion: @escaping LocationSearchResults) {
        var location: Location?
        dataStore?.read(statements: { db in
            let rs = try db.executeQuery(WeatherForecast.Queries.FindLocationById, values: [id])
            var locationJSON: String?
            while rs.next() {
                locationJSON = rs.string(forColumnIndex: 0)
                break
            }
            rs.close()
            location = try locationJSON?.decodeFromJSON(into: Location.self)
        }, { error in
            completion(error, location)
        })
    }
    
    func saveLocation(location: Location) {
        dataStore?.update(statements: { db, rollback in
            let countRS = try db.executeQuery(WeatherForecast.Queries.FindWeatherRecord, values: [location.name])
            var recordCount: Int = 0
            while countRS.next() {
                recordCount = countRS.long(forColumnIndex: 0)
            }
            countRS.close()
            if recordCount == 0 {
                let rs = try db.executeQuery(WeatherForecast.Queries.ReadMaxPosition, values: nil)
                var position: Int = 0
                while rs.next() {
                    position = rs.long(forColumnIndex: 0)
                }
                rs.close()
                position += 1
                let locationJSONData = try JSONEncoder().encode(location)
                guard let locationString = String(data: locationJSONData, encoding: .utf8) else {
                    throw WeatherDataModelError.malformedResponse
                }
                
                try db.executeUpdate(
                    WeatherForecast.Queries.CreateWeatherRecord,
                    values: [
                        location.identifier,
                        location.name,
                        position,
                        locationString
                    ]
                )
            }
        }) { error in
            if let error = error {
                print(error)
            } else {
                NotificationCenter.default.post(name: DataModelNotifications.LocationWasSaved,
                                                object: self,
                                                userInfo: [DataModelNotifications.LocationKey : location]
                )
            }
        }
    }
    
    func deleteLocation(location: Location, _ completion: @escaping (Error?) -> Void) {
        dataStore?.update(statements: { db, rollback in
            try db.executeUpdate(WeatherForecast.Queries.DeleteWeatherRecord, values: [location.identifier])
        }) { error in
            completion(error)
            NotificationCenter.default.post(name: DataModelNotifications.LocationWasDeleted, object: self, userInfo: [DataModelNotifications.LocationKey : location])
        }
    }
    
    func updateWeatherForecast(location: Location, _ completion: @escaping WeatherForecastResult) {
        weatherNetworkService?.forecast(query: location.name, completion: { error, response in
            if let error = error {
                completion(error, nil)
            }
            else {
                var weatherForecast: WeatherForecast?
                self.dataStore?.update(statements: {db, rollback in
                    guard let response = response else {
                        throw WeatherDataModelError.malformedResponse
                    }
                    try db.executeUpdate(WeatherForecast.Queries.UpdateWeatherForecast, values: [response, location.identifier])
                    weatherForecast = try response.decodeFromJSON(into: WeatherForecast.self)
                    weatherForecast?.location = location
                }) { error in
                    completion(error, weatherForecast)
                }
                
            }
        })
    }

}


