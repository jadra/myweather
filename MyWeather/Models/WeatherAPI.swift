//
//  WeatherAPI.swift
//  MyWeather
//
//  Created by john on 5/13/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation
import Alamofire

extension String {
    static let BASE_HOST = "api.apixu.com"
    static let BASE_URL = "http://api.apixu.com/v1/"
}


protocol WeatherService {
    func search(query: String, _ completion: @escaping StringCompletion)
    func forecast(query: String, completion: @escaping StringCompletion)
}

protocol WeatherAPIKeyProvider {
    var param : [String : String] { get }
}

typealias Completion = (Error?, Any?) -> Void
typealias StringCompletion = (Error?, String?) -> Void

enum WeatherAPIActions: String {
    case Search = "search.json"
    case CurrentWeather = "current.json"
    case Forecast = "forecast.json"
}


final class WeatherAPI: WeatherAPIKeyProvider, WeatherService {
    private var parameters: Dictionary<String, Any> = [:]
    struct Constants {
        static let KeyParamName = "key"
        static let QueryParamName = "q"
        static let ForecastDaysParamName = "days"
        static let ForecastDays = 10
    }
    
    private var baseURL: URL
    
    lazy var param: [String : String] = {
        return [Constants.KeyParamName : WeatherApiKey.Value]
    }()
    
    init(baseURL url: URL) {
        baseURL = url
        parameters.updateValue(param[Constants.KeyParamName]!, forKey: Constants.KeyParamName)
    }
    
    
    private func constructParams(with query: String) -> [String: Any] {
        var parms = parameters
        parms[Constants.QueryParamName] = query
        return parms
    }
    
    private func constructGET(_ url: String, _ parms: [String : Any], _ completion: @escaping StringCompletion)  {
        AF.request(url, method: .get, parameters: parms).responseString { response in
            switch response.result {
            case .success:
                completion(nil, response.value)
            case .failure(let error):
                completion(error, nil)
            }
        }
    }
    
    func search(query: String, _ completion: @escaping StringCompletion) {
        let url = baseURL.absoluteString + WeatherAPIActions.Search.rawValue
        let parms = constructParams(with: query)
        constructGET(url, parms, completion)
    }
        
    func forecast(query: String, completion: @escaping StringCompletion) {
        let url = baseURL.absoluteString + WeatherAPIActions.Forecast.rawValue
        var parms = constructParams(with: query)
        parms[Constants.ForecastDaysParamName] = Constants.ForecastDays
        constructGET(url, parms, completion)
    }
}

