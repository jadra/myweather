//
//  Models.swift
//  MyWeather
//
//  Created by john on 5/15/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

enum UnitSystem: Int {
    case imperial = 0
    case metric = 1
}

enum ConvertibleMeasurement: String {
    case wind
    case temp
    case windGust
    case pressure
    case precip
    case feelsLike
    case visibility
    case maxTemp
    case minTemp
    case avgTemp
    case maxWind
    case totalPrecip
    case avgVis
    
    static var descriptions: [ConvertibleMeasurement : [String]] {
        return [
            .wind : ["mph","kph"],
            .temp : ["F","C"],
            .windGust : ["mph","kph"],
            .pressure : ["in","mb"],
            .precip : ["in","mm"],
            .feelsLike : ["F","C"],
            .visibility : ["m","km"],
            .maxTemp : ["F", "C"],
            .minTemp : ["F", "C"],
            .avgTemp : ["F", "C"],
            .maxWind : ["mph","kph"],
            .totalPrecip : ["in","mm"],
            .avgVis : ["m","km"]
        ]
    }
    
}

protocol UnitSystemConvertable {
    var convertibleMeasurements: [ConvertibleMeasurement : [Double]] { get }
    
    func display(measurement: ConvertibleMeasurement, unitSystem: UnitSystem) -> Double
    
    func displayFormatted(measurement: ConvertibleMeasurement, unitSystem: UnitSystem) -> String
}

extension UnitSystemConvertable {
    func display(measurement: ConvertibleMeasurement, unitSystem: UnitSystem) -> Double {
        return convertibleMeasurements[measurement]![unitSystem.rawValue]
    }
    
    func displayFormatted(measurement: ConvertibleMeasurement, unitSystem: UnitSystem) -> String {
        let value = display(measurement: measurement, unitSystem: unitSystem)
        return String(describing: value).appending(" " + ConvertibleMeasurement.descriptions[measurement]![unitSystem.rawValue])
    }
}


struct Location: Codable {
    
    var name: String
    var locationDetail: Detail
    var lat: Double?
    var lon: Double?
    var identifier: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case name
        case lat
        case lon
        case identifier = "id"
    }
    
    struct Detail {
        var city: String {
            return valueAt(index: 0)
        }
        var region: String {
            return valueAt(index: 1)
        }
        var country: String {
            return valueAt(index: 2)
        }
        private let detail: [String]
        
        init(locationDetail: String) {
            self.detail = locationDetail.split(separator: ",").map { substring -> String in
                substring.trimmingCharacters(in: .whitespacesAndNewlines)
            }
        }
        
        private func valueAt(index offsetBy: Int) -> String {
            guard let idx = detail.index(
                detail.startIndex,
                offsetBy: offsetBy,
                limitedBy: detail.endIndex - 1
            ), detail.count > 0
            else {
                return "N/A"
            }
            return detail[idx]
        }
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        lat = try values.decode(Double.self, forKey: .lat)
        lon = try values.decode(Double.self, forKey: .lon)
        identifier = try values.decode(Int.self, forKey: .identifier)
        locationDetail = Detail(locationDetail: name)
        
    }
        
    init(name: String, lat: Double?, lon: Double?, identifier: Int) {
        self.name = name
        self.locationDetail = Detail(locationDetail: name)
        self.lat = lat
        self.lon = lon
        self.identifier = identifier
    }
    
    init(identifier: Int, name: String) {
        self.init(name: name, lat: nil, lon: nil, identifier: identifier)
    }
}

struct CurrentWeather: Codable, UnitSystemConvertable {
    var convertibleMeasurements: [ConvertibleMeasurement : [Double]] {
        return [
            .wind:         [windMPH, windKPH],
            .temp:         [tempF, tempC],
            .windGust:     [windGustMPH, windGustKPH],
            .pressure:     [pressureIN, pressureMB],
            .precip:       [precipIN, precipMM],
            .feelsLike:    [feelsLikeF, feelsLikeC],
            .visibility:   [visibilityM, visibilityKM]
        ]
    }

    var windDir: String
    var windKPH: Double
    var windMPH: Double
    var tempC: Double
    var tempF: Double
    var windGustKPH: Double
    var windGustMPH: Double
    var pressureMB: Double
    var pressureIN: Double
    var precipMM: Double
    var precipIN: Double
    var humidity: Int
    var feelsLikeC: Double
    var feelsLikeF: Double
    var visibilityKM: Double
    var visibilityM: Double
    var isDayTime: Int
    var condition: WeatherCondition
    
    var dayTime: Bool {
        return isDayTime == 0 ? false : true
    }
    
    enum CodingKeys: String, CodingKey {
        case windDir = "wind_dir"
        case windKPH = "wind_kph"
        case windMPH = "wind_mph"
        case tempC = "temp_c"
        case tempF = "temp_f"
        case windGustKPH = "gust_kph"
        case windGustMPH = "gust_mph"
        case pressureMB = "pressure_mb"
        case pressureIN = "pressure_in"
        case precipMM = "precip_mm"
        case precipIN = "precip_in"
        case humidity
        case feelsLikeC = "feelslike_c"
        case feelsLikeF = "feelslike_f"
        case visibilityKM = "vis_km"
        case visibilityM = "vis_miles"
        case isDayTime = "is_day"
        case condition
    }
    
}

struct WeatherForecast: Codable {
    
    var currentWeather: CurrentWeather
    var location: Location?
    var forecast: [String : [ForecastDay]]
    var days: [ForecastDay] {
        return forecast[ForecastKeys.days.rawValue] ?? []
    }
    
    enum CodingKeys: String, CodingKey {
        case currentWeather = "current"
        case forecast
    }
    
    enum ForecastKeys: String, CodingKey {
        case days = "forecastday"
    }
    
    struct Queries {
        static let CreateWeatherTable = """
            CREATE TABLE IF NOT EXISTS weather (
                identifier INT,
                name TEXT UNIQUE,
                position INT,
                location TEXT,
                current TEXT,
                forecast TEXT,
                PRIMARY KEY('identifier')
            ) WITHOUT ROWID;
        
        """
        
        static let ReadLocations = "SELECT identifier, name FROM weather"
        
        static let FindWeatherRecord = """
            SELECT count(name) AS recordCount FROM weather WHERE name = ?
        """
        
        static let FindLocationById = """
            SELECT json(location) as location from weather where identifier = ?
        """
        
        static let ReadMaxPosition = "SELECT max(position) AS position FROM weather"
        
        static let CreateWeatherRecord = """
            INSERT INTO weather(identifier, name, position, location) VALUES(?,?,?,?)
        """
        
        static let UpdateWeatherForecast = """
            UPDATE weather
            SET forecast = ?
            WHERE identifier = ?
        """
        static let DeleteWeatherRecord = "DELETE FROM weather where identifier = ?"
        
    }

    
}

struct Day: Codable, UnitSystemConvertable {
    var maxTempC: Double
    var maxTempF: Double
    var minTempC: Double
    var minTempF: Double
    var avgTempF: Double
    var avgTempC: Double
    var maxWindMPH: Double
    var maxWindKPH: Double
    var totalPrecipMM: Double
    var totalPrecipIN: Double
    var avgVisKM: Double
    var avgVisM: Double
    var avgHumidity: Double
    var uv: Double
    var condition: WeatherCondition
    
    var convertibleMeasurements: [ConvertibleMeasurement : [Double]] {
        return [
            .maxTemp:        [maxTempF, maxTempC],
            .minTemp:        [minTempF, minTempC],
            .avgTemp:        [avgTempF, avgTempC],
            .maxWind:        [maxWindMPH, maxWindKPH],
            .totalPrecip:    [totalPrecipIN, totalPrecipMM],
            .avgVis:         [avgVisKM, avgVisM]
        ]
    }
    
    
    enum CodingKeys: String, CodingKey {
        case maxTempC = "maxtemp_c"
        case maxTempF = "maxtemp_f"
        case minTempC = "mintemp_c"
        case minTempF = "mintemp_f"
        case avgTempC = "avgtemp_c"
        case avgTempF = "avgtemp_f"
        case maxWindMPH = "maxwind_mph"
        case maxWindKPH = "maxwind_kph"
        case totalPrecipMM = "totalprecip_mm"
        case totalPrecipIN = "totalprecip_in"
        case avgVisKM = "avgvis_km"
        case avgVisM = "avgvis_miles"
        case avgHumidity = "avghumidity"
        case uv
        case condition
    }
}

struct Astronomical: Codable {
    var sunrise: String
    var sunset: String
    var moonrise: String
    var moonset: String
    
    enum CodingKeys: String, CodingKey {
        case sunrise
        case sunset
        case moonrise
        case moonset
    }
}


struct ForecastDay: Codable {
    var date: String
    var day: Day
    var astro: Astronomical
    
    
    
    enum CodingKeys: String, CodingKey {
        case date
        case day
        case astro
    }
    
}

struct WeatherCondition: Codable {
    var iconPath: String
    var iconURL: URL? {
        return URL(string: "http:" + iconPath)
    }
    
    enum CodingKeys: String, CodingKey {
        case iconPath = "icon"
    }
}
