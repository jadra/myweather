//
//  DateFormatting.swift
//  MyWeather
//
//  Created by john on 6/10/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

protocol DateFormatting {
    var dateFormatter: DateFormatter { get }
    func formattedDateString(from dateString: String) -> String?
}

extension DateFormatting {
    var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd, EEE"
        return dateFormatter
    }
    
    func formattedDateString(from dateString: String) -> String? {
        let isoFormatter = ISO8601DateFormatter()
        isoFormatter.formatOptions = [.withFullDate]
        isoFormatter.timeZone = .current
        guard let isoDate = isoFormatter.date(from: dateString) else {
            return nil
        }
        
        let isToday = Calendar.current.isDateInToday(isoDate)
        let formattedString = dateFormatter.string(from: isoDate)
        
        return isToday ? formattedString.appending("(Today)") : dateFormatter.string(from: isoDate)
    }
}

class DateFormatService: DateFormatting {}
