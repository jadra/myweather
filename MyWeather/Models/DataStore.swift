//
//  PersistenceAPI.swift
//  MyWeather
//
//  Created by john on 5/15/19.
//  Copyright © 2019 john. All rights reserved.
//

// TODO: REFACTOR THE Weather Model actions out of this api!

import Foundation
import FMDB

protocol DataStorage {
    init(dbURL: URL?) throws
    func read(statements: @escaping (FMDatabase) throws -> Void, _ completion: @escaping (Error?) -> Void)
    func update(statements: @escaping (FMDatabase, UnsafeMutablePointer<ObjCBool>) throws -> Void, _ completion: @escaping (Error?) -> Void)
    func close()
}

final class DataStore: DataStorage {
    private var dbURL: URL?
    private var database: FMDatabase
    private var dbQueue: FMDatabaseQueue?
    private var queue: DispatchQueue
    
    private struct Constants {
        static let QueueName = "MyWeatherQueue"
    }
    
    init(dbURL: URL?) throws {
        self.dbURL = dbURL
        database = FMDatabase(url: dbURL)
        dbQueue = FMDatabaseQueue(path: dbURL?.absoluteString)
        queue = DispatchQueue(label: Constants.QueueName)
        try createSchema()
    }
    
    func close() {
        if database.open() {
            database.close()
        }
        dbQueue?.close()
    }
    
    deinit {
        close()
    }
    
    private func createSchema() throws {
        defer {
            database.close()
        }
        database.open()
        try database.executeUpdate(WeatherForecast.Queries.CreateWeatherTable, values: nil)
    }
        
    func update(statements: @escaping (FMDatabase, UnsafeMutablePointer<ObjCBool>) throws -> Void, _ completion: @escaping (Error?) -> Void) {
        defer {
            database.close()
        }
        database.open()
        queue.async {
            self.dbQueue?.inTransaction({ db, rollback in
                do {
                    try statements(db, rollback)
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                } catch {
                    rollback.pointee = true
                    DispatchQueue.main.async {
                        completion(error)
                    }
                }
            })
        }
    }
    
    func read(statements: @escaping (FMDatabase) throws -> Void, _ completion: @escaping (Error?) -> Void) {
        defer {
            database.close()
        }
        database.open()
        queue.async {
            self.dbQueue?.inDatabase{ db in
                do {
                    try statements(db)
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(error)
                    }
                }
            }
        }
    }
    
}
