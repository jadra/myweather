//
//  UIViewController+Extensions.swift
//  MyWeather
//
//  Created by john on 5/23/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlertOK(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alertView, animated: true, completion: nil)
    }
}
