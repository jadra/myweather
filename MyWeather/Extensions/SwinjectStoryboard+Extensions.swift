//
//  SwinjectStoryboard+Extensions.swift
//  MyWeather
//
//  Created by john on 5/20/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation
import SwinjectStoryboard


extension SwinjectStoryboard {
    
    @objc class func setup() {

        // It seems I need this in here to shut logging up about a resolution error
        // It may have something to do with logging but I don't see where I can quiet it
        // https://github.com/Swinject/Swinject/issues/218
        defaultContainer.storyboardInitCompleted(UINavigationController.self) {  _,_ in }

        defaultContainer.storyboardInitCompleted(WeatherForecastViewController.self) { resolver, controller in
            controller.viewModel = resolver.resolve(WeatherForecastViewModel.self)
            controller.viewModel.forecastDelegate = controller
            controller.viewModel.dateFormatter = resolver.resolve(DateFormatting.self)
        }
        
        defaultContainer.storyboardInitCompleted(ForecastDetailViewController.self) { resolver, controller in
            controller.viewModel = resolver.resolve(ForecastDetailViewModel.self)
            controller.viewModel.dateFormatter = resolver.resolve(DateFormatting.self)
        }
        
        defaultContainer.storyboardInitCompleted(SearchLocationsViewController.self) { resolver, controller in
            controller.viewModel = resolver.resolve(SearchLocationsViewModel.self)
        }
        
        defaultContainer.storyboardInitCompleted(LocationsViewController.self) { resolver, controller in
            controller.viewModel = resolver.resolve(LocationViewModel.self)
            controller.viewModel.delegate = controller
        }
        
        defaultContainer.register(WeatherForecastViewModel.self) { resolver in
            let model = WeatherForecastViewModel(weatherDataModel: resolver.resolve(WeatherDataModel.self)!)
            model.preferences = resolver.resolve(UserPreferences.self)
            return model
        }
        
        defaultContainer.register(ForecastDetailViewModel.self) { resolver in
            let model = ForecastDetailViewModel(weatherDataModel: resolver.resolve(WeatherDataModel.self)!)
            model.preferences = resolver.resolve(UserPreferences.self)
            return model
        }
        
        defaultContainer.register(SearchLocationsViewModel.self) { resolver in
            SearchLocationsViewModel(weatherDataModel: resolver.resolve(WeatherDataModel.self)!)
        }
        
        defaultContainer.register(LocationViewModel.self) { resolver in
            let model = LocationViewModel(weatherDataModel: resolver.resolve(WeatherDataModel.self)!)
            model.preferences = resolver.resolve(UserPreferences.self)
            return model
        }

        defaultContainer.register(WeatherDataModel.self) { resolver in
            let service = WeatherDataService()
            service.dataStore = resolver.resolve(DataStorage.self)
            service.weatherNetworkService = resolver.resolve(WeatherService.self)
            return service
        }.inObjectScope(.container)

        defaultContainer.register(DataStorage.self) { _ -> DataStorage in
            let appDir = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask)[0]
            try! FileManager.default.createDirectory(at: appDir, withIntermediateDirectories: true, attributes: nil)
            let url = appDir.appendingPathComponent("myweather.db")
            return try! DataStore(dbURL: url)
        }

        defaultContainer.register(WeatherService.self) { _ in
            WeatherAPI(baseURL: URL(string: String.BASE_URL)!)
        }
        
        defaultContainer.register(UserPreferences.self) { _ in
            return Preferences()
        }
        
        defaultContainer.register(DateFormatting.self) { _ in
            return DateFormatService()
        }.inObjectScope(.container)
                
    }

}
