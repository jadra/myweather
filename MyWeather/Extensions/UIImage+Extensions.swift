//
//  UIImage+Extensions.swift
//  MyWeather
//
//  Created by john on 6/5/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    static var placeholderImage: UIImage {
        return UIImage(imageLiteralResourceName: "question-mark-64")
    }
}
