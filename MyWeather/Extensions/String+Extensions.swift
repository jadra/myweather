//
//  String+Extensions.swift
//  MyWeather
//
//  Created by john on 5/13/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation


extension String {
    
    func decodeFromJSON<T>(into decodable: T.Type) throws -> T? where T: Decodable {
        guard let data = self.data(using: .utf8) else {
            return nil
        }
        let object = try JSONDecoder().decode(decodable.self, from: data)
        return object
    }
}
