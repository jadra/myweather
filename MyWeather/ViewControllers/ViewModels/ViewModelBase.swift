//
//  ViewModelBase.swift
//  MyWeather
//
//  Created by john on 5/20/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

protocol ViewModel {
    var weatherDataModel: WeatherDataModel { get }
    var preferences: UserPreferences! { get  set }
    var dateFormatter: DateFormatting! { get set }
    init(weatherDataModel: WeatherDataModel)
}

class ViewModelBase: ViewModel {
    private var dataModel: WeatherDataModel
    var dateFormatter: DateFormatting!
    var preferences: UserPreferences!
    var weatherDataModel: WeatherDataModel {
        return dataModel
    }
    
    required init(weatherDataModel: WeatherDataModel) {
        self.dataModel = weatherDataModel
    }
}
