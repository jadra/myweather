//
//  LocationViewModel.swift
//  MyWeather
//
//  Created by john on 6/3/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

protocol LocationViewModelDelegate {
    func locationsFetchDidBegin()
    func locationsFetchDidFinish(_ error: Error?)
}

class LocationViewModel: ViewModelBase {
    private var locations: [Location]?
    var delegate: LocationViewModelDelegate?
    var locationsCount: Int {
        return locations == nil ? 0 : locations!.count
    }
    
    func refresh() {
        delegate?.locationsFetchDidBegin()
        weatherDataModel.getLocations { error, locations in
            if let locations = locations {
                self.locations = locations
            }
            self.delegate?.locationsFetchDidFinish(error)
        }
    }
    
    func locationName(at indexPath: IndexPath) -> String? {
        if let locations = locations {
            return locations[indexPath.row].name
        }
        return nil
    }
    
    func locationSelected(at indexPath: IndexPath) {
        if let locations = locations {
            let location = locations[indexPath.row]
            preferences.currentLocation = location.identifier
            NotificationCenter.default.post(name: DataModelNotifications.LocationWasSaved,
                                            object: self,
                                            userInfo: [DataModelNotifications.LocationKey : location]
            )
        }
    }
    
    func locationDeleted(at indexPath: IndexPath, _ completion: @escaping (Error?) -> Void) {
        if let locations = locations {
            let location = locations[indexPath.row]
            weatherDataModel.deleteLocation(location: location) { error in
                completion(error)
            }
        }
    }
}
