//
//  ForecastDetailViewModel.swift
//  MyWeather
//
//  Created by john on 6/4/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

protocol ForecastDetailView {
    var forecast: ForecastDay! { get set }
}

class ForecastDetailViewModel: ViewModelBase, ForecastDetailView {
    var forecast: ForecastDay!
    
    private var day: Day {
        return forecast.day
    }
    
    private var system: UnitSystem {
        return UnitSystem(rawValue: preferences!.unitSystem)!
    }
    
    var weatherImageURL: URL? {
        return day.condition.iconURL
    }
    
    var date: String {
        return dateFormatter.formattedDateString(from: forecast.date)!
    }
    
    var minTemp: String {
        return day.displayFormatted(measurement: .minTemp, unitSystem: system)
    }
    
    var maxTemp: String {
        return day.displayFormatted(measurement: .maxTemp, unitSystem: system)
    }
    
    var avgTemp: String {
        return day.displayFormatted(measurement: .avgTemp, unitSystem: system)
    }
    
    var precip: String {
        return day.displayFormatted(measurement: .totalPrecip, unitSystem: system)
    }
    
    var wind: String {
        return day.displayFormatted(measurement: .maxWind, unitSystem: system)
    }
    
    var visibility: String {
        return day.displayFormatted(measurement: .avgVis, unitSystem: system)
    }
    
    var sunrise: String {
        return forecast.astro.sunrise
    }
    
    var sunset: String {
        return forecast.astro.sunset
    }
    
    var moonrise: String {
        return forecast.astro.moonrise
    }
    
    var moonset: String {
        return forecast.astro.moonset
    }
}
