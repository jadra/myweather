//
//  WeatherForecastViewModel.swift
//  MyWeather
//
//  Created by john on 5/21/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

protocol WeatherForecastUpdateDelegate {
    func updateDidBegin()
    func updateDidFinish(_ error: Error?)
}

struct DayInfo {
    var date: String?
    var maxTemp: String?
    var minTemp: String?
    var imageURL: URL?
}

class WeatherForecastViewModel: ViewModelBase {
    private var weatherForecast: WeatherForecast?
    private var refreshSemaphore: DispatchSemaphore
    var forecastDelegate: WeatherForecastUpdateDelegate?
    var sections: Int {
        return 2
    }
    var rowCounts: [Int] {
        return [1, weatherForecast == nil ? 0 : weatherForecast!.days.count]
    }
    
    var temperature: String {
        return textFor(measurement: .temp)
    }
    
    var feelsLike: String {
        return textFor(measurement: .feelsLike)
    }
    
    var precip: String {
        return textFor(measurement: .precip)
    }
    
    var pressure: String {
        return textFor(measurement: .pressure)
    }
    
    var wind: String {
        return textFor(measurement: .wind)
    }
    
    var windGust: String {
        return textFor(measurement: .windGust)
    }
    
    var visibility: String {
        return textFor(measurement: .visibility)
    }
    
    var city: String? {
        return weatherForecast?.location?.name
    }
    var unitSystemSelected: Int {
        return preferences!.unitSystem
    }
    var weatherImageURL: URL? {
        if let weather = weatherForecast {
            return weather.currentWeather.condition.iconURL
        }
        return nil
    }
    
    required init(weatherDataModel: WeatherDataModel) {
        refreshSemaphore = DispatchSemaphore(value: 1)
        super.init(weatherDataModel: weatherDataModel)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(locationWasSaved(notif:)),
                                               name: WeatherDataModelNotifications.LocationWasSaved,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(locationWasDeleted(notif:)), name: WeatherDataModelNotifications.LocationWasDeleted, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    private func updateForecast(location: Location) {
        forecastDelegate?.updateDidBegin()
        weatherDataModel.updateWeatherForecast(location: location) { error, forecast in
            self.weatherForecast = forecast
            self.forecastDelegate?.updateDidFinish(error)
        }
    }
    
    private func textFor(measurement: ConvertibleMeasurement) -> String {
        guard let weather = weatherForecast?.currentWeather,
              let unitSystem = UnitSystem(rawValue: preferences.unitSystem) else {
            return "N/A"
        }
        return weather.displayFormatted(measurement: measurement, unitSystem: unitSystem)
    }
    
    func unitSystemChanged(_ selectedSystem: Int) {
        if var prefs = preferences {
            prefs.unitSystem = selectedSystem
        }
    }
    
    func refresh() {
        refreshSemaphore.signal()
        if let preferences = preferences {
            weatherDataModel.getLocation(forId: preferences.currentLocation) { (error, location) in
                if let location = location {
                    self.updateForecast(location: location)
                } else {
                    self.forecastDelegate?.updateDidFinish(error)
                }
                self.refreshSemaphore.wait()
            }
        }
    }
    
    func day(at indexPath: IndexPath) -> DayInfo? {
        guard let weatherForecast = weatherForecast,
            let unitSystem = UnitSystem(rawValue: preferences.unitSystem)
        else {
            return nil
        }
        
        let aDay = weatherForecast.days[indexPath.row]
        var dayInfo = DayInfo()
        dayInfo.date =  dateFormatter.formattedDateString(from: aDay.date)
        dayInfo.maxTemp = aDay.day.displayFormatted(measurement: .maxTemp, unitSystem: unitSystem)
        dayInfo.minTemp = aDay.day.displayFormatted(measurement: .minTemp, unitSystem: unitSystem)
        dayInfo.imageURL = aDay.day.condition.iconURL
        
        return dayInfo
    }
    
    func forecastDay(index: Int) -> [String?] {
        var forecast: [String?] = []
        guard let weatherForecast = weatherForecast,
            let unitSystem = UnitSystem(rawValue: preferences.unitSystem)
         else {
            return forecast
        }
        
        let forecastDayInfo = weatherForecast.days[index]
        forecast.append(forecastDayInfo.date)
        forecast.append(forecastDayInfo.day.displayFormatted(measurement: .maxTemp, unitSystem: unitSystem))
        forecast.append(forecastDayInfo.day.displayFormatted(measurement: .minTemp, unitSystem: unitSystem))
        if let imageUrl = forecastDayInfo.day.condition.iconURL {
            forecast.append(imageUrl.absoluteString)
        } else {
            forecast.append(nil)
        }
        
        return forecast
    }
    
    func forecastSelected(at indexPath: IndexPath) -> ForecastDay {
        return weatherForecast!.days[indexPath.row]
    }
    
}

extension WeatherForecastViewModel {
    @objc func locationWasSaved(notif: Notification) {
        if let location = notif.userInfo?[WeatherDataModelNotifications.LocationKey] as? Location {
            preferences?.currentLocation = location.identifier
            updateForecast(location: location)
        }
    }
}

extension WeatherForecastViewModel {
    @objc func locationWasDeleted(notif: Notification) {
        if  let location = notif.userInfo?[WeatherDataModelNotifications.LocationKey] as? Location,
            let forecast = weatherForecast,
            let currLocation = forecast.location,
            currLocation.identifier == location.identifier {
            weatherForecast = nil
            preferences.currentLocation = 0
        }
        
        forecastDelegate?.updateDidFinish(nil)
    }
}
