//
//  SearchLocationsViewModel.swift
//  MyWeather
//
//  Created by john on 5/21/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation


class SearchLocationsViewModel: ViewModelBase {
    private var locations: [Location]?
    var results: [Location] {
        guard let locations = locations else {
            return []
        }
        return locations
    }
    
    func search(location: String, _ completion: @escaping (Error?, Bool) -> Void) {
        self.weatherDataModel.searchForLocation(query: location) { error, locationsFound in
            self.locations = locationsFound
            completion(error, self.results.count > 0 ? true : false)
        }
    }
    
    func locationsCount() -> Int {
        return results.count
    }
    
    func locationSelected(atIndex index: Int) {
        weatherDataModel.saveLocation(location: results[index])
    }
    
}
