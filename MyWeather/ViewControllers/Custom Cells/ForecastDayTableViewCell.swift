//
//  ForecastDayTableViewCell.swift
//  MyWeather
//
//  Created by john on 6/3/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class ForecastDayTableViewCell: UITableViewCell {
    static let Identifier = "ForecastCell"
    static let Size: CGFloat = 60
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
