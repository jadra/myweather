//
//  CurrentWeatherTableViewCell.swift
//  MyWeather
//
//  Created by john on 6/2/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class CurrentWeatherTableViewCell: UITableViewCell {
    static let Identifier = "CurrentWeatherCell"
    static let Size: CGFloat = 200
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var tempSwitch: UISegmentedControl!
    @IBOutlet weak var precipLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var windGustLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
