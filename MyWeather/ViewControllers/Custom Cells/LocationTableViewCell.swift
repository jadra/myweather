//
//  LocationTableViewCell.swift
//  MyWeather
//
//  Created by john on 5/22/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {
    static let Identifier = "LocationsCell"
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
