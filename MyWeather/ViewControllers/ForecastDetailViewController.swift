//
//  ForecastDetailViewController.swift
//  MyWeather
//
//  Created by john on 6/4/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class ForecastDetailViewController: UIViewController {
    var viewModel: ForecastDetailViewModel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var avgTempLabel: UILabel!
    @IBOutlet weak var precipLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var moonriseLabel: UILabel!
    @IBOutlet weak var moonsetLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        dateLabel.text = viewModel.date
        minTempLabel.text = viewModel.minTemp
        maxTempLabel.text = viewModel.maxTemp
        avgTempLabel.text = viewModel.avgTemp
        precipLabel.text = viewModel.precip
        windLabel.text = viewModel.wind
        visibilityLabel.text = viewModel.visibility
        sunriseLabel.text = viewModel.sunrise
        sunsetLabel.text = viewModel.sunset
        moonriseLabel.text = viewModel.moonrise
        moonsetLabel.text = viewModel.moonset
    }

}
