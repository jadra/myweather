//
//  LocationsViewController.swift
//  MyWeather
//
//  Created by john on 5/18/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit
import PKHUD

class LocationsViewController: UITableViewController {
    static let CellIdentifier = "LocationCell"
    var viewModel: LocationViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        navigationItem.rightBarButtonItem = editButtonItem
        viewModel.refresh()
    }
}

//MARK: UITableViewDataSource extensions
extension LocationsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.locationsCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = LocationsViewController.CellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) ?? UITableViewCell(style: .default, reuseIdentifier: identifier)
        if let name = viewModel.locationName(at: indexPath) {
            cell.textLabel?.text = name
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: "delete") { (action, view, actionPerformed) in
            HUD.show(.progress)
            self.viewModel.locationDeleted(at: indexPath, { error in
                HUD.hide()
                if let error = error {
                    self.showAlertOK(title: "Could Not Delete!", message: error.localizedDescription)
                }
            })
            actionPerformed(true)
        }
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
}

//MARK: UITableViewDelegate extensions
extension LocationsViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.locationSelected(at: indexPath)
    }
}

//MARK: LocationViewModelDelegate extensions
extension LocationsViewController: LocationViewModelDelegate {
    func locationsFetchDidBegin() {
        HUD.show(.progress)
    }
    
    func locationsFetchDidFinish(_ error: Error?) {
        HUD.hide()
        if let error = error {
            showAlertOK(title: "Locations error", message: error.localizedDescription)
        } else {
            tableView.reloadData()
        }
    }
}
