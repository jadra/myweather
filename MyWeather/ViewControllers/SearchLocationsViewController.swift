//
//  SearchLocationsViewController.swift
//  MyWeather
//
//  Created by john on 5/18/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit
import PKHUD

class SearchLocationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: SearchLocationsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.accessibilityLabel = "LocationSearch"
        navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
        searchBar.delegate = self
    }
}


extension SearchLocationsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            HUD.show(.progress)
            viewModel!.search(location: text) { error, hasResults in
                HUD.hide()
                if let error = error {
                    self.showAlertOK(title: "Error", message: error.localizedDescription)
                } else {
                    if !hasResults {
                        self.showAlertOK(title: "No results!", message: "Your search returned no results")
                    } else {
                        searchBar.resignFirstResponder()
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
}

extension SearchLocationsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel!.locationsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = LocationTableViewCell.Identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? LocationTableViewCell ?? LocationTableViewCell(style: .default, reuseIdentifier: cellId)
        
        let locationDetail = viewModel!.results[indexPath.row].locationDetail
        cell.cityLabel.text = locationDetail.city
        cell.regionLabel.text = locationDetail.region
        cell.countryLabel.text = locationDetail.country
        
        return cell
    }
}

extension SearchLocationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.locationSelected(atIndex: indexPath.row)
        dismiss(animated: true, completion: nil)
    }
}
