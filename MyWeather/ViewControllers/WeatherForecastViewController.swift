//
//  WeatherLocationsViewController.swift
//  MyWeather
//
//  Created by john on 5/14/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import PKHUD


class WeatherForecastViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel: WeatherForecastViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup() {
        viewModel.refresh()
    }
    
    @IBAction func unwindToMainMenu(sender: UIStoryboardSegue) {}
    
    
    @IBAction func refreshButtonPressed(_ sender: UIBarButtonItem) {
        viewModel.refresh()
    }
    
    
    @IBAction func unitSystemChanged(_ sender: UISegmentedControl) {
        viewModel.unitSystemChanged(sender.selectedSegmentIndex)
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if  let cell = sender as? UITableViewCell,
            let indexPath = tableView.indexPath(for: cell), segue is ForecastDetailSegue {
            (segue as! ForecastDetailSegue).forecast = viewModel.forecastSelected(at: indexPath)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let selectedRowIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedRowIndexPath, animated: false)
        }
    }
}


extension WeatherForecastViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowCounts[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let identifier = CurrentWeatherTableViewCell.Identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CurrentWeatherTableViewCell ?? CurrentWeatherTableViewCell(style: .default, reuseIdentifier: identifier)
            cell.cityLabel.text = viewModel.city
            cell.tempLabel.text = viewModel.temperature
            cell.feelsLikeLabel.text = viewModel.feelsLike
            cell.precipLabel.text = viewModel.precip
            cell.pressureLabel.text = viewModel.pressure
            cell.windLabel.text = viewModel.wind
            cell.windGustLabel.text = viewModel.windGust
            cell.visibilityLabel.text = viewModel.visibility
            cell.tempSwitch.selectedSegmentIndex = viewModel.unitSystemSelected
            if let url = viewModel.weatherImageURL {
                cell.weatherImage.af_setImage(withURL: url)
            } else {
                cell.weatherImage.image =  Image.placeholderImage
            }
            return cell
        } else {
            let identifier = ForecastDayTableViewCell.Identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ForecastDayTableViewCell ?? ForecastDayTableViewCell(style: .default, reuseIdentifier: identifier)
            if let dayInfo = viewModel.day(at: indexPath) {
                cell.dateLabel.text = dayInfo.date
                cell.maxTempLabel.text = dayInfo.maxTemp
                cell.minTempLabel.text = dayInfo.minTemp
                if let url = dayInfo.imageURL {
                    cell.weatherImage.af_setImage(withURL: url)
                } else {
                    cell.weatherImage.image = Image.placeholderImage
                }
            }
            return cell
        }
    }
}

extension WeatherForecastViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? CurrentWeatherTableViewCell.Size : ForecastDayTableViewCell.Size
    }    
}

extension WeatherForecastViewController: WeatherForecastUpdateDelegate {
    func updateDidBegin() {
        HUD.show(.progress)
    }
    
    func updateDidFinish(_ error: Error?) {
        HUD.hide()
        if let error = error {
            showAlertOK(title: "Error!", message: error.localizedDescription)
        } else {
            tableView.reloadData()
        }
    }
}
