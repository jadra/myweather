//
//  ForecastDetailSegue.swift
//  MyWeather
//
//  Created by john on 6/4/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class ForecastDetailSegue: UIStoryboardSegue {
    var forecast: ForecastDay!
    
    override func perform() {
        if let destination = destination as? ForecastDetailViewController {
            destination.viewModel.forecast = forecast
        }
        super.perform()
    }
}
