# My Weather App

This is a basic Weather forecast application with minimal features. So just the basics like: current weather, forecast,
location search/save/recall/delete. No frills. Just function. :)

1) Clone this repo.
2) Get yourself an api key from [Apixu](https://www.apixu.com)
3) copy the contents of **WeatherApiKey.template.swift** to a file named **WeatherApiKey.swift** under MyWeather folder.
4) Paste your key into the Value assignment (`static let Value = "Your Key Here"`)
5) `run carthage update --platform iOS`
