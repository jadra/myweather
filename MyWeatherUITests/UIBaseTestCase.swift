//
//  UIBaseTestCase.swift
//  MyWeatherUITests
//
//  Created by john on 6/10/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest

let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")

class UIBaseTestCase: XCTestCase {
    

    override func tearDown() {
        deleteApp(appName: "MyWeather")
    }
    
    func deleteApp(appName: String) {
        XCUIApplication().terminate()
        
        // Force delete the app from the springboard
        let icon = springboard.icons[appName]
        if icon.exists {
            icon.press(forDuration: 2.0)
            
            icon.buttons["DeleteButton"].tap()
            sleep(2)
            springboard.alerts["Delete “\(appName)”?"].buttons["Delete"].tap()
            sleep(2)
            
            XCUIDevice.shared.press(.home)
        }
    }
}
