//
//  MyWeatherUITests.swift
//  MyWeatherUITests
//
//  Created by john on 5/26/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest


class MyWeatherUITests: UIBaseTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        XCUIDevice.shared.orientation = .portrait
        
    }


    func testLocationSearchNoResults() {
        
        let app = XCUIApplication()
        let predicate = NSPredicate(format: "exists == true")
        let expect = XCTNSPredicateExpectation(predicate: predicate, object: app.alerts["No results!"])

        app/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["LocationSearchNavBar"].children(matching: .searchField).element.typeText("X")
        app/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        wait(for: [expect], timeout: 5)
    }
    
    func testLocationSearchReturnsResults() {
        
        let app = XCUIApplication()
        let predicate = NSPredicate(format: "count > 0")
        let table = app.tables["SearchResultsTable"]
        let expect = XCTNSPredicateExpectation(predicate: predicate, object: table.children(matching: .cell))

        app.navigationBars["My Weather"].buttons["Search"].tap()
        app.navigationBars["LocationSearchNavBar"].children(matching: .searchField).element.typeText("Woodbridge, NJ")
        app/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        
        wait(for: [expect], timeout: 5)
    }
    
    func testShowCurrentWeather() {
        
        let app = XCUIApplication()
        app.navigationBars["My Weather"].buttons["Search"].tap()
        app.navigationBars["LocationSearchNavBar"].children(matching: .searchField).element.typeText("Woodbridge, NJ")
        app.buttons["Search"].tap()
        app.tables["SearchResultsTable"].cells.containing(.staticText, identifier:"Woodbridge").staticTexts["New Jersey"].tap()
        let cityRegionLabelExists = app.tables.staticTexts["CityRegionLabel"].waitForExistence(timeout: 5.0)
        XCTAssertTrue(cityRegionLabelExists)
    }
    
    func testShowCurrentWeatherTodayDetail() {
        
        let app = XCUIApplication()
        app.navigationBars["My Weather"].buttons["Search"].tap()
        app.navigationBars["LocationSearchNavBar"].children(matching: .searchField).element.typeText("Woodbridge, NJ")
        app.buttons["Search"].tap()
        app.tables["SearchResultsTable"].cells.containing(.staticText, identifier:"Woodbridge").staticTexts["New Jersey"].tap()
        app.tables.cells["ForecastCell"].firstMatch.tap()
        let detailDateLabelExists = app.staticTexts["ForecastDetailDateLabel"].waitForExistence(timeout: 5.0)
        XCTAssertTrue(detailDateLabelExists)
    }
}
