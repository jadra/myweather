//
//  LocationViewModelTests.swift
//  MyWeatherTests
//
//  Created by john on 6/6/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest
@testable import MyWeather


class LocationViewModelTests: DataStoreBase {
    var viewModel: LocationViewModel!
    
    override func setUp() {
        super.setUp()
        let dataModel = WeatherDataService()
        dataModel.dataStore = dataStore
        viewModel = LocationViewModel(weatherDataModel: dataModel)
        viewModel.preferences = TestPrefs(unitSystem: 0, currentLocation: 0)
    }

    override func tearDown() {
        super.tearDown()
        viewModel.delegate = nil
        viewModel.preferences = nil
        viewModel = nil
        
    }
    
    func testLocationsCountIsZero() {
        XCTAssertTrue(viewModel.locationsCount == 0)
    }

    func testLocationCountIsGreaterThanZero() {
        let expect = expectation(description: "Should get test locations")
        var delegate = ViewModelDelegate(viewModel: viewModel)
        
        delegate.completion = { error in
            XCTAssertNil(error)
            XCTAssertTrue(self.viewModel.locationsCount > 0)
            expect.fulfill()
        }
        
        viewModel.delegate = delegate
        
        locations { locs in
            XCTAssertTrue(locs.count > 0)
            self.viewModel.refresh()
        }
        
        wait(for: [expect], timeout: 1.0)
    }
    
    func testLocationNameIsNil() {
        XCTAssertNil(viewModel.locationName(at: IndexPath(row: 0, section: 0)))
    }
    
    func testLocationNameReturnsString() {
        let expect = expectation(description: "Should fetch name of location")
        var delegate = ViewModelDelegate(viewModel: viewModel)
        
        delegate.completion = { error in
            XCTAssertNil(error)
            let name = self.viewModel.locationName(at: IndexPath(row: 0, section: 0))
            XCTAssertNotNil(name)
            XCTAssertEqual(name, "Rahway, New Jersey, United States of America")
            expect.fulfill()
        }
        
        viewModel.delegate = delegate
        
        locations { locs in
            self.viewModel.refresh()
        }
        wait(for: [expect], timeout: 1.0)
    }
    
    func testLocationSelection() {
        let locationsExpect = expectation(description: "Should get test locations")
        let completionExpect = expectation(description: "Selection should be made")
        let notifName = DataModelNotifications.LocationWasSaved
        
        let notifExpect = expectation(forNotification: notifName, object: viewModel) { notif -> Bool in
            guard let object = notif.object as? Location else {
                return false
            }
            return object.identifier == 2613155
        }
        
        var delegate = ViewModelDelegate(viewModel: viewModel)
        
        delegate.completion = { error in
            XCTAssertNil(error)
            self.viewModel.locationSelected(at: IndexPath(row: 0, section: 0))
            notifExpect.fulfill()
            completionExpect.fulfill()
        }
        
        viewModel.delegate = delegate

        
        locations { locs in
            self.viewModel.refresh()
            locationsExpect.fulfill()
        }
        
        wait(for: [locationsExpect,completionExpect,notifExpect], timeout: 5.0)
    }
    
    func testLocationDeletion() {
        let locationsExpect = expectation(description: "Should get test locations")
        let completionExpect = expectation(description: "Deletion should be called")
        let notifName = DataModelNotifications.LocationWasDeleted
        
        let notifExpect = expectation(forNotification: notifName, object: viewModel) { notif -> Bool in
            guard let object = notif.object as? Location else {
                return false
            }
            return object.identifier == 2613155
        }
        
        var delegate = ViewModelDelegate(viewModel: viewModel)
        
        delegate.completion = { error in
            XCTAssertNil(error)
            self.viewModel.locationSelected(at: IndexPath(row: 0, section: 0))
            self.viewModel.locationDeleted(at: IndexPath(row: 0, section: 0), { error in
                XCTAssertNil(error)
                notifExpect.fulfill()
                completionExpect.fulfill()
            })
        }
        
        viewModel.delegate = delegate
        
        
        locations { locs in
            self.viewModel.refresh()
            locationsExpect.fulfill()
        }
        
        wait(for: [locationsExpect,completionExpect,notifExpect], timeout: 5.0)

    }
    
}

//MARK: Empty UserPrefences compliance
struct TestPrefs: UserPreferences {
    var unitSystem: Int
    var currentLocation: Int
}


//MARK: Delegate utility struct
struct ViewModelDelegate: LocationViewModelDelegate {
    
    let viewModel: LocationViewModel
    var completion: ((Error?) -> Void)?
    
    init(viewModel: LocationViewModel) {
        self.viewModel = viewModel
    }
    
    func locationsFetchDidBegin() {}
    
    func locationsFetchDidFinish(_ error: Error?) {
        completion?(error)
    }
}
