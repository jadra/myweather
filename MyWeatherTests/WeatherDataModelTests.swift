//
//  PersistenceAPITests.swift
//  MyWeatherTests
//
//  Created by john on 5/15/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest
@testable import MyWeather

class WeatherDataModelTests: DataStoreBase {
    
    var weatherDataService: WeatherDataModel?
    
    override func setUp() {
        super.setUp()
        createWeatherDataService()
    }

    override func tearDown() {
        super.tearDown()
        weatherDataService = nil
    }
    
    func createWeatherDataService() {
        let weatherAPI = WeatherAPI(baseURL: URL(string: String.BASE_URL)!)
        weatherDataService = WeatherDataService()
        weatherDataService?.dataStore = dataStore
        weatherDataService?.weatherNetworkService = weatherAPI
    }
    
    func testSearchSuccess() {
        let expect = expectation(description: "Search should return results")
        
        weatherDataService?.searchForLocation(query: TestConstants.ValidLocation) { (error, locations) in
            XCTAssertNil(error)
            XCTAssertNotNil(locations)
            expect.fulfill()
        }
        
        wait(for: [expect], timeout: 1.0)
    }


    func testSaveLocationSuccess() {
        let expect = expectation(description: "Location should be saved successfully")
        
        weatherDataService?.searchForLocation(query: TestConstants.ValidLocation) { (error, locations) in
            XCTAssertNil(error)
            XCTAssertNotNil(locations)
            XCTAssertTrue(locations!.count > 0)
            self.weatherDataService?.saveLocation(location: locations![0])
            expect.fulfill()
        }
        
        wait(for: [expect], timeout: 1.0)
    }
    
    func testUpdateWeatherForecast() {
        let expect = expectation(description: "Weather forecast should update successfully")
        
        weatherDataService?.searchForLocation(query: TestConstants.ValidLocation) { (error, locations) in
            XCTAssertNil(error)
            XCTAssertNotNil(locations)
            XCTAssertTrue(locations!.count > 0)
            
            self.weatherDataService?.updateWeatherForecast(location: locations![0], { error, forecast in
                XCTAssertNil(error)
                XCTAssertNotNil(forecast)
                expect.fulfill()
            })
        }
        
        wait(for: [expect], timeout: 2.0)
    }
    
    func testFetchLocationsSuccess() {
        
        let expect = expectation(description: "Weather forecast should update successfully")
        
        let bundle = Bundle(for: WeatherDataModelTests.self)
        let locationsJSON = bundle.url(forResource: "locations", withExtension: "json")
        let locations = try! JSONSerialization.jsonObject(with: Data(contentsOf: locationsJSON!), options: []) as! [Dictionary<String, Any>]

        
        weatherDataService?.dataStore?.update(statements: { (db, rollback) in
            try db.executeUpdate("DELETE FROM weather", values: nil)
            for location in locations {
                let id = location["id"] as! Int
                try db.executeUpdate("INSERT INTO weather(identifier, name, position, location) VALUES(?,?,?,?)",
                                     values: [id, location["name"]!, "",""])
                
            }
        }) { error in
            self.weatherDataService?.getLocations({ (error, locations) in
                XCTAssertNil(error)
                XCTAssertNotNil(locations)
                XCTAssertTrue(locations!.count == 10)
                expect.fulfill()
            })
        }
        
        wait(for: [expect], timeout: 2.0)
    }
    
}
