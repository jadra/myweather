//
//  DateFormattingTests.swift
//  MyWeatherTests
//
//  Created by john on 6/10/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest
@testable import MyWeather

class DateFormattingTests: XCTestCase {

    override func setUp() {}

    override func tearDown() {}

    func testDateFormatValid() {
        let dateString = "2019-06-11"
        let formattedDateString = DateFormatService().formattedDateString(from: dateString)
        XCTAssertNotNil(formattedDateString)
        XCTAssertEqual(formattedDateString, "2019-06-11, Tue")
    }
    
    func testDateFormatShowsToday() {
        let date = Date()
        let isoDateFormatter = ISO8601DateFormatter()
        let dateString = isoDateFormatter.string(from: date)
        let formattedDateString = DateFormatService().formattedDateString(from: dateString)
        XCTAssertNotNil(formattedDateString)
        XCTAssert(formattedDateString!.contains("(Today)"))
    }
    
}
