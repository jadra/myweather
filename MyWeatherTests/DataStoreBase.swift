//
//  DataStoreBase.swift
//  MyWeatherTests
//
//  Created by john on 5/22/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest
@testable import MyWeather

class DataStoreBase: XCTestCase {
    var dataStore: DataStorage!
    
    override func setUp() {
        dataStore = try! DataStore(dbURL: TestConstants.DBURL)
        locations { locations in
            
        }
    }
    
    override func tearDown() {
        dataStore.close()
        deleteDB()
        dataStore = nil
    }
    
    fileprivate func deleteDB() {
        try! FileManager.default.removeItem(at: TestConstants.DBURL)
    }
    
    func locations(_ completion: @escaping ([Location]) -> Void) {
        let bundle = Bundle(for: DataStoreBase.self)
        let locationsJSON = bundle.url(forResource: "locations", withExtension: "json")
        let locationsDict = try! JSONSerialization.jsonObject(with: Data(contentsOf: locationsJSON!), options: []) as! [Dictionary<String, Any>]
        
        var locations = [Location]()
        
        for locationDict in locationsDict {
            let location = Location(identifier: locationDict["id"] as! Int, name: locationDict["name"] as! String)
            locations.append(location)
        }

        dataStore.update(statements: { (db, rollback) in
            for location in locations {
                try db.executeUpdate("DELETE from weather", values: nil)
                try db.executeUpdate("INSERT INTO weather(identifier, name, position, location) VALUES(?,?,?,?)",
                                     values: [location.identifier, location.name, "",""])
            }
        }) { error in
            XCTAssertNil(error)
            completion(locations)
        }

    }
}
