//
//  SearchLocationsViewModelTests.swift
//  MyWeatherTests
//
//  Created by john on 5/22/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest
@testable import MyWeather

class SearchLocationsViewModelTests: DataStoreBase {
    var viewModel: SearchLocationsViewModel?
    
    override func setUp() {
        super.setUp()
        let dataModel = WeatherDataService()
        let weatherService = WeatherAPI(baseURL: URL(string: String.BASE_URL)!)
        dataModel.weatherNetworkService = weatherService
        dataModel.dataStore = dataStore
        viewModel = SearchLocationsViewModel(weatherDataModel: dataModel)
    }

    override func tearDown() {
        super.tearDown()
        viewModel = nil
    }
    
    func testSearchReturnsResults() {
        let expect = expectation(description: "Search should return result(s)")
        viewModel?.search(location: "woodbridge") { error, hasResults in
            XCTAssertNil(error)
            XCTAssertTrue(hasResults)
            expect.fulfill()
        }
        wait(for: [expect], timeout: 2.0)
    }
    
    func testSearchReturnsNoResults() {
        let expect = expectation(description: "Search should not return result(s)")
        viewModel?.search(location: "X") { error, hasResults in
            XCTAssertNil(error)
            XCTAssertFalse(hasResults)
            expect.fulfill()
        }
        wait(for: [expect], timeout: 2.0)
    }
    
    func testLocationsCountZero() {
        XCTAssertTrue(viewModel?.locationsCount() == 0)
    }
    
    func testSearchResultSavedSuccess() {
        let expect = expectation(description: "Location should be saved")
        viewModel?.search(location: "woodbridge") { error, hasResults in
            XCTAssertNil(error)
            XCTAssertTrue(hasResults)
            let firstLocation = self.viewModel!.results[0]
            
            self.viewModel?.locationSelected(atIndex: 0)
            self.dataStore?.read(statements: { db in
                let rs = try! db.executeQuery(WeatherForecast.Queries.FindWeatherRecord, values: [firstLocation.name])
                XCTAssert(rs.next())
                XCTAssert(rs.long(forColumnIndex: 0) == 1)
                rs.close()
                expect.fulfill()
            }, { error in
                XCTAssertNil(error)
            })
            
        }
        wait(for: [expect], timeout: 2.0)
    }
}
