//
//  DataStoreTests.swift
//  MyWeatherTests
//
//  Created by john on 5/17/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest
@testable import MyWeather

class DataStoreTests: DataStoreBase {
    
    func testReadSuccess() {
        let expect = expectation(description: "Read should select successfully")
        var hasNext = false
        dataStore?.read(statements: { db in
            let rs = try db.executeQuery("select json('[1,2,3]')", values: nil)
            hasNext = rs.next()
        }) { error in
            XCTAssertNil(error)
            XCTAssertTrue(hasNext)
            expect.fulfill()
        }
        
        wait(for: [expect], timeout: 1.0)
    }

    func testUpdateSuccess() {
        let expect = expectation(description: "Update should create table weather successfully")
        dataStore?.update(statements: { db, rollback in
            try db.executeUpdate("CREATE TABLE aTable(someCol INT)", values: nil)
            try db.executeQuery("select count(someCol) from aTable", values: nil)
        }) { error in
            XCTAssertNil(error)
            expect.fulfill()
        }
        
        wait(for: [expect], timeout: 1.0)
    }
}
