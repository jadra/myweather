//
//  Constants.swift
//  MyWeatherTests
//
//  Created by john on 5/17/19.
//  Copyright © 2019 john. All rights reserved.
//

import Foundation

struct TestConstants {
    static let DBURL = FileManager.default.temporaryDirectory.appendingPathComponent("test.db")
    static let ValidLocation = "Woodbridge, NJ"
    static let InvalidLocation = "X"
    static let WoodbridgeForecastPath = "woodbridge-nj-forecast-weather.json"
    static let WoodbridgeLocationsPath = "locations.json"
}


