//
//  NetworkStub.swift
//  MyWeatherTests
//
//  Created by john on 6/9/19.
//  Copyright © 2019 john. All rights reserved.
//

//import Foundation
import XCTest
@testable import MyWeather
import OHHTTPStubs


class MyWeatherNetworkStub {
    static let JsonContentType = ["Content-Type":"application/json"]
    var WoodbridgeForecastPath: String!  {
        return OHPathForFile(TestConstants.WoodbridgeForecastPath, type(of: self))
    }
    var WoodbridgeLocationsPath: String! {
        return OHPathForFile(TestConstants.WoodbridgeLocationsPath, type(of: self))
    }
    
    func stubRequests() {
        
        OHHTTPStubs.onStubActivation { request, descriptor, response in
            print("Activated request url is \(request.url?.absoluteString ?? "none")")
        }

        stub(condition: isHost(String.BASE_HOST)) { request -> OHHTTPStubsResponse in
            XCTAssertNotNil(request.url)
            let components = URLComponents(url: request.url!, resolvingAgainstBaseURL: false)
            let q = components!.queryItems?.filter({ item -> Bool in
                return item.name == WeatherAPI.Constants.QueryParamName
            }).first!
            
            let lastPath = request.url!.lastPathComponent
        
            if lastPath == WeatherAPIActions.Forecast.rawValue &&
                q!.value == TestConstants.ValidLocation {
                return OHHTTPStubsResponse(fileAtPath: self.WoodbridgeForecastPath!,
                                           statusCode: 200,
                                           headers: MyWeatherNetworkStub.JsonContentType)
            }
            else if lastPath == WeatherAPIActions.Search.rawValue &&
                q!.value == TestConstants.InvalidLocation {
                return OHHTTPStubsResponse(jsonObject: [], statusCode: 200, headers: nil)
            }
            
            return OHHTTPStubsResponse(fileAtPath: self.WoodbridgeLocationsPath!,
                                       statusCode: 200,
                                       headers: MyWeatherNetworkStub.JsonContentType)
        }
    }
    
}
