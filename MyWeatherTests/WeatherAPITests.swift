//
//  WeatherAPITests.swift
//  MyWeatherTests
//
//  Created by john on 5/13/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import MyWeather

class WeatherAPITests: XCTestCase {

    override func setUp() {
        super.setUp()
        MyWeatherNetworkStub().stubRequests()
    }

    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testSearchSuccess() {
        let expect = expectation(description: "Search request should succeed")
        
        let api = WeatherAPI(baseURL: URL(string: String.BASE_URL)!)
        api.search(query: TestConstants.ValidLocation) { (error, result) in
            XCTAssertNil(error)
            XCTAssertNotNil(result)
            expect.fulfill()
        }
        wait(for: [expect], timeout: 1.0)
    }
    
    func testSearchNoResults() {
        let expect = expectation(description: "Search with invalid location returns no results")
        
        let api = WeatherAPI(baseURL: URL(string: String.BASE_URL)!)
        api.search(query: TestConstants.InvalidLocation) { error, result in
            XCTAssertNil(error)
            XCTAssertTrue(result == "[]")
            expect.fulfill()
        }
        wait(for: [expect], timeout: 1.0)
    }

    func testForecastSuccess() {
        let expect = expectation(description: "Forecast request should succeed")
        
        let api = WeatherAPI(baseURL: URL(string: String.BASE_URL)!)
        api.forecast(query: TestConstants.ValidLocation) { error, result in
            XCTAssertNil(error)
            XCTAssertNotNil(result)
            expect.fulfill()
        }
        wait(for: [expect], timeout: 2.0)
    }

    
    
}
