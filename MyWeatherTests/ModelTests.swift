//
//  ModelTests.swift
//  MyWeatherTests
//
//  Created by john on 5/22/19.
//  Copyright © 2019 john. All rights reserved.
//

import XCTest
@testable import MyWeather

class ModelTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testLocationsDetailAllFields() {
        let location = Location(identifier: 2000, name: "NY,NY,USA")
        XCTAssertTrue(location.locationDetail.city == "NY")
        XCTAssertTrue(location.locationDetail.region == "NY")
        XCTAssertTrue(location.locationDetail.country == "USA")
    }
    
    func testLocationsDetailCityAndRegionFields() {
        let location = Location(identifier: 2000, name: "NY,NY")
        XCTAssertTrue(location.locationDetail.city == "NY")
        XCTAssertTrue(location.locationDetail.region == "NY")
        XCTAssertTrue(location.locationDetail.country == "N/A")
    }
    
    func testLocationsDetailCityField() {
        let location = Location(identifier: 2000, name: "NY")
        XCTAssertTrue(location.locationDetail.city == "NY")
        XCTAssertTrue(location.locationDetail.region == "N/A")
        XCTAssertTrue(location.locationDetail.country == "N/A")
    }
    
    func testLocationsDetailNoFields() {
        let location = Location(identifier: 2000, name: "")
        XCTAssertTrue(location.locationDetail.city == "N/A")
        XCTAssertTrue(location.locationDetail.region == "N/A")
        XCTAssertTrue(location.locationDetail.country == "N/A")

    }
    
    func testConvertibleTemp() {
        
        var weather = CurrentWeather(windDir: "N", windKPH: 0, windMPH: 0, tempC: 100, tempF: 212, windGustKPH: 0, windGustMPH: 0, pressureMB: 0, pressureIN: 0, precipMM: 0, precipIN: 0, humidity: 0, feelsLikeC: 0, feelsLikeF: 0, visibilityKM: 0, visibilityM: 0, isDayTime: 0, condition: WeatherCondition(iconPath: ""))
        XCTAssert(weather.display(measurement: .temp, unitSystem: .imperial) == 212)
        XCTAssert(weather.display(measurement: .temp, unitSystem: .metric) == 100)
        weather.tempF = 32
        weather.tempC = 0
        XCTAssert(weather.display(measurement: .temp, unitSystem: .imperial) == 32)
        XCTAssert(weather.display(measurement: .temp, unitSystem: .metric) == 0)
        
    }
}
